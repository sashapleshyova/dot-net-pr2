﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ConsoleApp1
{
    public delegate bool IsPositive(double a);

    public static class LINQExtension
    {
        public static double SumExt(this IEnumerable<double> source)
        {
            return source.Sum();
        }

        public static bool IsInt(this IEnumerable<char> source)
        {
            var output = source.Where(c => '0' <= c && c <= '9');
            if (output.Count() == source.Count())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IEnumerable<double> FindPositive(this IEnumerable<double> source)
        {
            return source.Where(c => 0 <= c);
        }

        public static IEnumerable<double> FindPositive(this IEnumerable<double> source, IsPositive _isPositive)
        {
            return source.Where(item => _isPositive(item));
        }

        public static IEnumerable<double> FindPositiveLINQ(this IEnumerable<double> source)
        {
            return from item in source
                where 0 <= item
                select item;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("- Задание 1");
            const int length = 1000000;
            double[] numbers = new double[length];
            int value = -111;
            for (int i = 0; i < length; i++)
            {
                numbers[i] = value;
            }

            Console.WriteLine("Сумма элементов массива: " + numbers.SumExt());

            Console.WriteLine("\n- Задание 2");
            string strNumber = "-168";
            Console.WriteLine(strNumber + (strNumber.IsInt()
                ? " - положительное целое число"
                : " - не положительное и(или) не целое число"));
            strNumber = "23";
            Console.WriteLine(strNumber + (strNumber.IsInt()
                ? " - положительное целое число"
                : " - не положительное и(или) не целое число"));
            strNumber = "2,3";
            Console.WriteLine(strNumber + (strNumber.IsInt()
                ? " - положительное целое число"
                : " - не положительное и(или) не целое число"));

            Console.WriteLine("\n- Задание 3");
          
            List <long> times = new List<long>{};

            for (int i = 0; i < 11; i++)
            {
                Stopwatch time = Stopwatch.StartNew(); 
                numbers.FindPositive().Count();
                time.Stop();
                times.Add(time.ElapsedTicks);
            }

            times.Sort();
            Console.WriteLine("Скорость 1 поиска (метод, реализующий поиск напрямую): " + times.ElementAt(6) + " тиков");
            times.Clear();
            

            for (int i = 0; i < 11; i++)
            {
                 Stopwatch time = Stopwatch.StartNew(); 
                 numbers.FindPositive(MyFindPositive).Count();
                 time.Stop();
                 times.Add(time.ElapsedTicks);
            }
            times.Sort();
            Console.WriteLine("Скорость 2 поиска (метод, которому условие поиска передается через делегат): " + times.ElementAt(6) + " тиков");
            times.Clear();
            
            
            IsPositive Handler = delegate(double a) { return a >= 0; };
            for (int i = 0; i < 11; i++)
            {
                Stopwatch time = Stopwatch.StartNew(); 
                numbers.FindPositive(Handler).Count();
                time.Stop();
                times.Add(time.ElapsedTicks);
            }

            times.Sort();
            Console.WriteLine("Скорость 3 поиска (метод, которому условие поиска передается через делегат в виде анонимного метода): " + times.ElementAt(6) + " тиков");
            times.Clear();


            for (int i = 0; i < 11; i++)
            {
                Stopwatch time = Stopwatch.StartNew();
                numbers.FindPositive(item => item >= 0).Count();
                  time.Stop();
                  times.Add(time.ElapsedTicks);
            }
          
            times.Sort();
            Console.WriteLine(
                "Скорость 4 поиска (метод, которому условие поиска передается через делегат в виде лямбда-выражения): " + times.ElementAt(6) + " тиков");
            times.Clear();
            

            for (int i = 0; i < 11; i++)
            {
                Stopwatch time = Stopwatch.StartNew(); 
                numbers.FindPositiveLINQ().Count();
                time.Stop();
                times.Add(time.ElapsedTicks);
            }

            times.Sort();
            Console.WriteLine("Скорость 5 поиска (LINQ-выражение): " + times.ElementAt(6) + " тиков");
        }

        private static bool MyFindPositive(double a)
        {
            return a >= 0;
        }
    }
}